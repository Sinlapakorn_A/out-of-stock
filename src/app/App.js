import React, { useState, useEffect } from "react";
import { useLazyQuery, gql } from '@apollo/client';
import { makeStyles } from '@material-ui/core/styles';
import SvgIcon from '@material-ui/core/SvgIcon';
import { createGlobalStyle } from "styled-components";

import { ImageGrid, ProfileGrid } from "../components/Grid/Grid.style"
import { CircleImage } from "../components/Image/Image.style"
import LazyImage from "../components/Image/LazyImage";
import Tabs from "../components/Tab/Tab";
import SearchBar from "../components/SearchBar/SearchBar";
import TabPanel from "../components/Tab/TabPanel";

import { device } from "../constant/device"

import "./App.css"

const SNEAKER_LIST_QUERY = gql`
  query($searchQuery: String) {
    sneaker(search: $searchQuery) {
      id
      media {
        smallImageUrl
      }
    }
  }
`;

const useStyles = makeStyles((theme) => ({
  root: {
    '& > svg': {
      margin: theme.spacing(2),
    },
  },
}));

const GridOn = (props) => (
  <SvgIcon {...props}>
    <path d="M20 2H4c-1.1 0-2 .9-2 2v16c0 1.1.9 2 2 2h16c1.1 0 2-.9 2-2V4c0-1.1-.9-2-2-2zM8 20H4v-4h4v4zm0-6H4v-4h4v4zm0-6H4V4h4v4zm6 12h-4v-4h4v4zm0-6h-4v-4h4v4zm0-6h-4V4h4v4zm6 12h-4v-4h4v4zm0-6h-4v-4h4v4zm0-6h-4V4h4v4z" />
  </SvgIcon>
);

const TvLive = (props) => (
  <SvgIcon {...props}>
    <path d="M21 6h-7.59l3.29-3.29L16 2l-4 4-4-4-.71.71L10.59 6H3c-1.1 0-2 .89-2 2v12c0 1.1.9 2 2 2h18c1.1 0 2-.9 2-2V8c0-1.11-.9-2-2-2zm0 14H3V8h18v12zM9 10v8l7-4z"/>
  </SvgIcon>
);

const BookmarkBorder = (props) => (
  <SvgIcon {...props}>
    <path d="M17 3H7c-1.1 0-1.99.9-1.99 2L5 21l7-3 7 3V5c0-1.1-.9-2-2-2zm0 15l-5-2.18L7 18V5h10v13z"/>
  </SvgIcon>
);

const AssignmentInd = (props) => (
  <SvgIcon {...props} className="outline-icon">
    <path d="M19 3h-4.18C14.4 1.84 13.3 1 12 1c-1.3 0-2.4.84-2.82 2H5c-1.1 0-2 .9-2 2v14c0 1.1.9 2 2 2h14c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2zm-7 0c.55 0 1 .45 1 1s-.45 1-1 1-1-.45-1-1 .45-1 1-1zm0 4c1.66 0 3 1.34 3 3s-1.34 3-3 3-3-1.34-3-3 1.34-3 3-3zm6 12H6v-1.4c0-2 4-3.1 6-3.1s6 1.1 6 3.1V19z"/>
  </SvgIcon>
);

const Global = createGlobalStyle`
  body {
    margin: 0px;
    background-color: rgba(250,250,250,1);
    main {
      margin: 0px;
      max-width: 935px;
      padding: 30px 20px 0;
      margin: 0 auto 30px;
      @media only screen and ${device.laptop} {
        padding: 0;
      }
    }
  } 
`;

export default function App() {
  const classes = useStyles();
  const [searchTerm, setSearchTerm] = useState("");


  const [getSneaker, { loading, error, data }] = useLazyQuery(SNEAKER_LIST_QUERY);

  const handleSearchChange = (event) => {
    setSearchTerm(event.target.value);
  };

  useEffect(() =>  {
    getSneaker({
      variables: { searchQuery: searchTerm }
    })
  }, []); // eslint-disable-line react-hooks/exhaustive-deps

  if (loading) return <p>Loading...</p>;
  if (error) return <p>Error :(</p>;

  return (
    <div className={classes.root + " App"}>
      <Global />
      <SearchBar
        searchTerm={searchTerm}
        handleSearchChange={handleSearchChange}
        getSneaker={getSneaker}
      />
      <main>
        <ProfileGrid>
          <CircleImage
            src="/profile.jpg"
            className="grid-image"
          />
          <div className="grid-header overflow-hidden">
            <h1 className="overflow-truncate">Out of Stock</h1>
          </div>
          <div className="grid-description">
            <div className="font-bold">
              Description:
            </div>
            <div className="pt-1">
              Not immediately available for sale or use.
            </div>
            <div className="pt-1">
              Not having goods of a specified kind immediately available.
            </div>
          </div>
        </ProfileGrid>
        <div className="container">
        <Tabs>
          <TabPanel name="posts" key="posts" icon={<GridOn />}>
            <ImageGrid>
              { (data && data.sneaker)
                ? data.sneaker.map(sneaker => (
                <LazyImage
                  key={sneaker.id}
                  // src={`https://picsum.photos/1000/1000?random=${i}`}
                  src={sneaker.media.smallImageUrl}
                  alt={sneaker.id}
                />
                ))
                : "Something went wrong"
              }
            </ImageGrid>
          </TabPanel>
            <TabPanel name="video" key="video" icon={<TvLive />}>
            Not available
          </TabPanel>
          <TabPanel name="saved" key="saved" icon={<BookmarkBorder />}>
            Not available
          </TabPanel>
          <TabPanel name="tag" key="tag" icon={<AssignmentInd />}>
            Not available
          </TabPanel>
        </Tabs>
      </div>
      </main>
    </div>
  );
}
