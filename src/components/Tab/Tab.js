import React, { useEffect, useState } from "react";
import TabPanel from "./TabPanel";
import styled from "styled-components";

const TabHeader = styled.ul`
  border-top: 1px solid rgb(219, 219, 219);
  align-items: center;
  display: flex;
  flex-direction: row;
  list-style: none;
  justify-content: center;
  text-transform: uppercase;
  padding: 0px;
  margin: 0px;
  height: 52px;
`;

const Tab = styled.li`
  cursor: pointer;
  height: 100%;
  display: flex;
  flex-direction: row;
  align-items: center;
  color: rgb(219, 219, 219);
  &:not(:last-child) {
    margin-right: 60px;
  }
  &.active {
  border-top: 1px solid rgb(38, 38, 38);
  color: rgb(38, 38, 38);
  margin-top: -1px;
    .outline-icon {
      stroke: rgb(38, 38, 38);
    }
  }
  .outline-icon {
    color: #FFFFFF;
    stroke: rgb(219, 219, 219);
    stroke-width: 1.5px
  }
`;

const Tabs = (props) => {
  const { children } = props;
  const [tabHeader, setTabHeader] = useState([]);
  const [tabIcon, setTabIcon] = useState({});
  const [childContent, setChildConent] = useState({});
  const [active, setActive] = useState("");
  useEffect(() => {
    const headers = [];
    const icon = {};
    const childCnt = {};
    React.Children.forEach(children, (element) => {
      if (!React.isValidElement(element)) return;
      const { name } = element.props;
      headers.push(name);
      icon[name] = element.props.icon
      childCnt[name] = element.props.children;
    });
    setTabHeader(headers);
    setTabIcon(icon);
    setActive(headers[0]);
    setChildConent({ ...childCnt });
  }, [props, children]);

  const changeTab = (name) => {
    setActive(name);
  };

  return (
    <div>
      <TabHeader>
        {tabHeader.map((item) => (
          <Tab
            onClick={() => changeTab(item)}
            key={item}
            className={item === active ? "active" : ""}
          >
            <span className="d-flex flex-end">
              {tabIcon[item]}
              <span className="show-laptop pl-1">
                {item}
              </span>
            </span>
          </Tab>
        ))}
      </TabHeader>
      <div>
        {Object.keys(childContent).map((key) => {
          if (key === active) {
            return <div>{childContent[key]}</div>;
          } else {
            return null;
          }
        })}
      </div>
    </div>
  );
};

Tabs.propTypes = {
  children: function (props, propName, componentName) {
    const prop = props[propName];

    let error = null;
    React.Children.forEach(prop, function (child) {
      if (child.type !== TabPanel) {
        error = new Error(
          "`" + componentName + "` children should be of type `TabPanel`."
        );
      }
    });
    return error;
  }
};

export default Tabs;