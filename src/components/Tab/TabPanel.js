import React from "react";
import PropTypes from "prop-types";

const TabPanel = (props) => {
  return <div>{props.childern}</div>;
};

TabPanel.propTypes = {
  name: PropTypes.string
};

export default TabPanel;