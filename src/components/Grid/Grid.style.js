import styled from "styled-components";
import { device } from "../../constant/device"

export const ImageGrid = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr 1fr;
  grid-gap: 28px;
  @media only screen and ${device.laptop} {
    grid-gap: 3px;
    border-top: 1px solid rgb(219, 219, 219);
  }
`;

export const ProfileGrid = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr 1fr;
    grid-template-rows: 75px 75px;
    grid-template-areas:
    "image header header"
    "image description description";
    row-gap: 0px;
    column-gap: 30px;
  padding-bottom: 44px;
  @media only screen and ${device.laptop} {
    grid-gap: 28px;
    padding: 16px 16px 24px;
    grid-template-columns: 77px 1fr;
    grid-template-rows: auto;
    grid-template-areas:
      "image header"
      "description description";
  }
`;