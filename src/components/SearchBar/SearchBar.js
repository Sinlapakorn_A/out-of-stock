import React from "react";
import styled from "styled-components";
import SvgIcon from '@material-ui/core/SvgIcon';

const GridOn = (props) => (
  <SvgIcon {...props}>
    <path d="M15.5 14h-.79l-.28-.27C15.41 12.59 16 11.11 16 9.5 16 5.91 13.09 3 9.5 3S3 5.91 3 9.5 5.91 16 9.5 16c1.61 0 3.09-.59 4.23-1.57l.27.28v.79l5 4.99L20.49 19l-4.99-5zm-6 0C7.01 14 5 11.99 5 9.5S7.01 5 9.5 5 14 7.01 14 9.5 11.99 14 9.5 14z" />
  </SvgIcon>
);

const Nav = styled.nav`
  background-color: rgba(255,255,255,1);
  height: 54px;
  display: flex;
  align-items: center;
  justify-content: center;
  z-index: 1;
  width: 100%;
  position: fixed;
  top: 0;
  left: 0;
  .search-box {
    position: relative;
    // svg {
    //   color: rgba(219,219,219,1);
    //   font-size: 14px;
    //   height: 100%;
    //   position: absolute;
    //   margin-left: 10px
    // }
    input {
      font-size: 14px;
      background-color: rgba(250,250,250,1);
      border: solid 1px rgba(219,219,219,1);
      height: 20px;
      padding: 3px 10px;
      // padding: 3px 10px 3px 34px;
    }
  }
  button {
    height: 28px;
    color: rgba(120,120,120,1);
    background-color: rgba(219,219,219,1);
    border: solid 1px rgba(219,219,219,1);
  }
`;

const NavBar = ({ searchTerm, handleSearchChange, getSneaker}) => {
  return (
    <div>
      <div style={{ height: "54px" }}></div>
      <Nav>
        <div className="search-box">
          {/* <GridOn /> */}
          <input
            type="text"
            placeholder="Search"
            value={searchTerm}
            onChange={handleSearchChange}
            onBlur={() => getSneaker({
              variables: { searchQuery: searchTerm }
            })}
            onKeyDown={(e) => {
              if(e.key === 'Enter') {
                getSneaker({
                  variables: { searchQuery: searchTerm }
                })
              }
            }}
          />
        </div>
        <button onClick={() => getSneaker({
          variables: { searchQuery: searchTerm }
        })}>
            <GridOn />
        </button>
      </Nav>
    </div>
  );
};

NavBar.propTypes = {
};

export default NavBar;