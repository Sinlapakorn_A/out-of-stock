import styled from "styled-components";
import { device } from "../../constant/device"

export const CircleImage = styled.img`
  width: 150px;
  height: 150px;
  margin: auto;
  border-radius: 50%;
  @media only screen and ${device.laptop} {
    width: 77px;
    height: 77px;
  }
`;