const size = {
  laptop: '735px',
}

export const device = {
  laptop: `(max-width: ${size.laptop})`,
};