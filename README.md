<!-- GETTING STARTED -->
## Getting Started
This is an example of how you may give instructions on setting up your project locally.
To get a local copy up and running follow these simple example steps.

### Demo
   [http://139.5.144.28:3005/](http://139.5.144.28:3005/)

### Prerequisites
This is a server project if you need to use in locally.
* Qraphql Server Out Of Stock
  Get a project at [https://gitlab.com/Sinlapakorn_A/graphql-server-out-of-stock.git](https://gitlab.com/Sinlapakorn_A/graphql-server-out-of-stock.git)

### Installation
1. Clone the repo
   ```sh
   git clone https://gitlab.com/Sinlapakorn_A/out-of-stock.git
   ```
2. Install NPM packages
   ```sh
   npm install
   ```

### Run
1. Start "Graphql Server Out Of Stock" Application at port 4000
   or
   Use .env file
   ```sh
   REACT_APP_SERVER_IP = "http://139.5.144.28:4000"
   ```
2. Start this application
   ```sh
   npm start
   ```
3. Then open [http://localhost:3000/](http://localhost:3000/) to see your app.